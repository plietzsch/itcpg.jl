#* Functions for handling the Network Data Dictionary (NDD) of PowerModels.jl
#*------------------------------------------------------------------------------

#=
Builds a network data dictionary conform with PowerModels.jl from several files containing certain input data:
model - whether to calculate AC- or DC-PF (:ac or :dc)
pgfile - .RAW or .m file that contains the detailed grid data (e.g. generator and load information etc.)
busfile - .csv file that contains geographic locations of all buses
branchfile - optional .csv file that contains transmission line lengths
nddfile - .jld2 file in which the final network data dictionary will be saved
=#

"""
    build_network_data(file::String; kwargs...)

Reads a .RAW or .m `file` containing the data of a power grid and calculates the power flow according to the chosen model (see below). Returns a network data dictionary conform with the [PowerModels Network Data Format](https://lanl-ansi.github.io/PowerModels.jl/stable/network-data/) that also contains the calculated power flow solution.
"""
function build_network_data(
        file::String;
        locfile = "", lenfile = "", outfile = "",
        pf_model = :ac,
        aggregate_generators = false, # whether to merge parallel generators
        kwargs...
    )

    ### Read grid data and calculate operation point
    network_data = calc_pf(file, pf_model=pf_model; kwargs...)

    ### Check validity generator dispatches
    check_gen_bounds(network_data, mode=:aggregated, if_violation=:warning)
    check_gen_bounds(network_data, mode=:individual, if_violation=:warning)

    if aggregate_generators == true
        aggregate_generators!(network_data)
    end
    
    ### Add additional data to network_data, if given
    if isempty(locfile) == false
        add_locs!(network_data, locfile) # add geographic bus locations
    end
    if isempty(lenfile) == false
        add_tl_lengths!(network_data, lenfile)
    end

    add_tl_voltages!(network_data) # add transmission line voltages

    if isempty(outfile) == false
        save(outfile, "network_data", network_data) # save network data
    end

    return network_data
end

function aggregate_generators!(network_data::Dict{String,<:Any})    
    ### Find buses with active generators
    pv_buses = [
        b["index"] for b in values(network_data["bus"]) if b["bus_type"] ∈ [2,3]
    ]
    
    ### Go through PV-buses and aggregate active generators
    aggregated_gens = Dict{String,Any}()
    for (i, b) in enumerate(pv_buses)
        ### Find active generators connected to b
        active_gens = [
            g for g in values(network_data["gen"]) if 
            g["gen_bus"] == b && g["gen_status"] == 1
        ]
        ### Initialize new entry in aggregated_gens
        aggregated_gens["$i"] = Dict(
            "vg" => active_gens[1]["vg"],
            "mbase" => 0.0, # not needed for power flow analysis
            "source_id" => Any["generator", b, "1 "],
            "model" => active_gens[1]["model"],
            "shutdown" => active_gens[1]["shutdown"],
            "startup" => active_gens[1]["startup"],
            "index" => i,
            "cost" => active_gens[1]["cost"],
            "gen_status" => 1,
            "gen_bus" => b,
            "ncost" => active_gens[1]["ncost"],
            "pg" => 0.0,
            "pmin" => 0.0,
            "pmax" => 0.0,
            "qg" => 0.0,
            "qmin" => 0.0,
            "qmax" => 0.0
        )
        ### Add up generator dispatches
        for gen in active_gens
            aggregated_gens["$i"]["pg"] += gen["pg"]
            aggregated_gens["$i"]["pmin"] += gen["pmin"]
            aggregated_gens["$i"]["pmax"] += gen["pmax"]
            aggregated_gens["$i"]["qg"] += gen["qg"]
            aggregated_gens["$i"]["qmin"] += gen["qmin"]
            aggregated_gens["$i"]["qmax"] += gen["qmax"]
        end
    end

    ### Replace individual generators with aggregated generators
    network_data["gen"] = aggregated_gens

    return nothing
end

#*------------------------------------------------------------------------------

#=
Reads the geographic bus locations (longitude and latitude coordinates in degrees) from a .csv file and adds them to the network data dictionary (NDD). Returns the resulting NDD, in which each bus has a "bus_lat" and "bus_lon" entry. The columns containg the longitude and latitude coordinates in the .csv file should be called "SubLongitude" and "SubLatitude", respectively.
=#
function add_locs!(network_data::Dict{String,<:Any}, csvfile::String)
    pos = Dict{String,Any}(
        "baseMVA"  => 100.0,
        "bus" => Dict{String,Any}(), 
        "per_unit" => true
    )

    BusData::DataFrame = CSV.File(csvfile) |> DataFrame
    N = size(BusData, 1) # number of buses
    sizehint!(pos["bus"], N)

    for bus in eachrow(BusData)
        pos["bus"][string(bus[:Number])] = Dict(
            "bus_lon" => bus[:SubLongitude], 
            "bus_lat" => bus[:SubLatitude]
        )
    end
    update_data!(network_data, pos) # add positions to network_data
    
    return network_data
end

#*------------------------------------------------------------------------------

#=
Reads transmission line lengths from a .csv file and adds them to the network data dictionary.
=#
function add_tl_lengths!(network_data::Dict{String,<:Any}, csvfile::String)
    len = Dict{String,Any}(
        "baseMVA"  => 100.0,
        "branch" => Dict{String,Any}(), 
        "per_unit" => true
    )

    BranchData::DataFrame = CSV.File(csvfile) |> DataFrame
    N = size(BranchData, 1) # number of branches
    sizehint!(len["branch"], N)

    for branch in eachrow(BranchData)
        from, to = branch[:BusNumFrom], branch[:BusNumTo]
        indices = findall(
            b -> ((b["f_bus"], b["t_bus"]) == (from, to) 
            || (b["f_bus"], b["t_bus"]) == (to, from)), network_data["branch"]
        )
        for index in indices
            len["branch"][index] = Dict("length" => branch[:GICLineDistanceKm])
        end
    end
    update_data!(network_data, len) # add positions to network_data
    
    return network_data
end

#*------------------------------------------------------------------------------

#=
Determines voltage levels of transmission lines and adds them to the network data dictionary.
=#
function add_tl_voltages!(network_data::Dict{String,<:Any})
    voltages = Dict{String,Any}(
        "baseMVA"  => 100.0,
        "branch" => Dict{String,Any}(), 
        "per_unit" => true
    )
    L = length(network_data["branch"])
    sizehint!(voltages["branch"], L)

    for (i, branch) in network_data["branch"]
        if branch["transformer"] == true
            ### Transformers have a transmission line voltage of 0 kV
            voltages["branch"][i] = Dict(
                "tl_voltage" => 0. 
            )
        else
            ### Transmission lines get base voltage of from bus
            from = branch["f_bus"]
            voltages["branch"][i] = Dict(
                "tl_voltage" => network_data["bus"][string(from)]["base_kv"]
            )
        end
    end
    update_data!(network_data, voltages)

    return network_data
end

#*------------------------------------------------------------------------------

#=
Returns the bus types of all buses in the NDD in form an ordered dictionary. The different bus types are "Generator", "Load and generator", "Load" and "Empty bus". The ordering is fixed so that the dictionary can be used to plot the buses in a specific order.
=#
#? How to call "empty" buses? Load and generator buses in a substation are connected to these empty buses? Is there only one empty bus per substation?
function get_bustypes(network_data::Dict{String,<:Any})
    ### Active loads
    load = unique(
        [l["load_bus"] for l in collect(values(network_data["load"]))
        if l["status"] == 1]
    )

    ### Active generators
    gen = unique(
        [g["gen_bus"] for g in collect(values(network_data["gen"]))
        if g["gen_status"] == 1]
    )
    
    ### Buses with both load and generation
    load_and_gen = [i for i in load if i in gen]
    
    ### Slack bus
    slack = [
        b["index"] for b in collect(values(network_data["bus"])) 
        if b["bus_type"] == 3
    ]

    ### Remove buses with load and generation from pure loads and generators
    filter!(i -> i ∉ load_and_gen, load)
    filter!(i -> i ∉ load_and_gen, gen)
    ### Remove slack bus from other arrays
    filter!(i -> i ∉ slack, load)
    filter!(i -> i ∉ slack, gen)
    filter!(i -> i ∉ slack, load_and_gen)
    
    ### Empty buses with neither load nor generation
    empty = unique(
        [b["index"] for b in collect(values(network_data["bus"]))
        if b["index"] ∉ vcat(load, gen, load_and_gen, slack)]
    )

    ### Check whether the right number of bus types was obtained
    sum = (
        length(load) + length(gen) + length(load_and_gen) + length(empty) + length(slack)
    )
    N::Int64 = length(network_data["bus"]) # number of buses
    @assert sum == N "$sum bus types were obtained instead of $N"

    return OrderedDict(
        "Generator" => gen,
        "Load and generator" => load_and_gen,
        "Load" => load,
        "Slack" => slack,
        "Empty bus" => empty       
    )
end

#*------------------------------------------------------------------------------

#=
Returns the (string) indices of transmission lines in the network data dictionary that would be considered as underground lines depending on the used maximum length of underground lines and minimum MW-load served. If mode = :sum, the sum of MW-loads of both ends of a line has to exceed min_MW_load, and if mode = :single, one end alone has to exceed min_MW_load.
=#
function get_underground_tl(
        network_data::Dict{String,<:Any}, 
        max_length = 12.875, # in km
        min_MW_load = 2.; # in per-unit (assumed 100 MW base)
        mode = :sum # :sum or :single
    )

    MW_loads = get_MW_loads(network_data) # dictionary of MW loads

    underground_tl = []
    if mode == :single # one end alone has to exceed min_MW_load
        filter!(load -> last(load) >= min_MW_load, MW_loads)
        large_MW_loads = collect(keys(MW_loads))
        ### Go through branches and identify underground transmission lines
        for (i, branch) in network_data["branch"]
            if branch["transformer"] == false && branch["length"] < max_length
                from, to = branch["f_bus"], branch["t_bus"]
                if from in large_MW_loads || to in large_MW_loads
                    push!(underground_tl, i)
                end
            end
        end
    elseif mode == :sum # both ends together have to exceed min_MW_load
        ### Go through branches and identify underground transmission lines
        for (i, branch) in network_data["branch"]
            if branch["transformer"] == false && branch["length"] < max_length
                from, to = branch["f_bus"], branch["t_bus"]
                sum_MW_load = 0.
                if haskey(MW_loads, from)
                    sum_MW_load += MW_loads[from]
                end
                if haskey(MW_loads, to)
                    sum_MW_load += MW_loads[to]
                end
                if sum_MW_load >= min_MW_load
                    push!(underground_tl, i)
                end
            end
        end
    else
        throw(ArgumentError("Unknown mode $mode."))
    end

    return underground_tl
end

#=
Returns a dictionary with load bus indices (Int64) as keys and their respective MW load as values.
=#
function get_MW_loads(network_data::Dict{String,<:Any})
    load_dict = Dict{Int64,Float64}()
    for (i, load) in network_data["load"]
        sub_dict = Dict(load["load_bus"] => load["pd"])
        merge!(+, load_dict, sub_dict)
    end
    return load_dict
end

function get_total_load(network_data::Dict{String,<:Any})
    total_MW_load = 0.
    total_Mvar_load = 0.
    total_MVA_load = 0.

    for load in [l for l in values(network_data["load"]) if l["status"] == 1]
        total_MW_load += load["pd"]
        total_Mvar_load += load["qd"]
        total_MVA_load += sqrt(load["pd"]^2 + load["qd"]^2)
    end

    return total_MW_load, total_Mvar_load, total_MVA_load
end

function get_inactive_elements(network_data::Dict{String,<:Any})
    inactive_components = Dict{String,Array{Int64,1}}(
        "bus" => [
            b["index"] for b in values(network_data["bus"]) if 
            b["bus_type"] == 4
        ],
        "gen" => [
            g["index"] for g in values(network_data["gen"]) if 
            g["gen_status"] != 1
        ],
        "load" => [
            l["index"] for l in values(network_data["load"]) if
            l["status"] != 1
        ],
        "destroyed_br" => [
            br["index"] for br in values(network_data["branch"]) if 
            br["br_status"] != 1 && haskey(br, "failure_reason") == true &&
            br["failure_reason"] == "wind"
        ],
        "overloaded_br" => [
            br["index"] for br in values(network_data["branch"]) if 
            br["br_status"] != 1 && haskey(br, "failure_reason") == true &&
            br["failure_reason"] == "overload"
        ],
        "overproduction_br" => [
            br["index"] for br in values(network_data["branch"]) if
            br["br_status"] != 1 && haskey(br, "failure_reason") == true &&
            br["failure_reason"] == "overproduction"
        ],
        "other_br" => [
            br["index"] for br in values(network_data["branch"]) if
            br["br_status"] != 1 && haskey(br, "failure_reason") == false
        ]
    )
end

#*------------------------------------------------------------------------------

# Returns an array of tuples that each contain information about specific branches of interest. The tuples contain the respective branch index, the loading of the branch and a boolean value set to `1`, if the branch is a transformer, or `0`, if the branch is a transmission line. 
"""
    get_branches(network_data::Dict{String,<:Any}; kwargs...)

Returns the indices of branches in `network_data` that are loaded (flow/capacity) by at least `min_loading` (see **keyword arguments**).

# Keyword Arguments

`pf_type` can be set to `"MVA-loading"` (default), `"MW-loading"` or `"Mvar-loading"` and specifies whether the loading due to apparent (MVA), active (MW) or reactive power (Mvar) should be considered. If `network_data["pf_model"] => :dc` meaning that it was last updated using a DC power flow solution, `pf_type` is automatically set to `"MW-loading"`.

`min_loading` is a `Float64` (default `0.0`) that specifies the minimum loading of branches to be returned (according to `pf_type`). Only branches with a loading equal or above `min_loading` will be returned. Hence, `min_loading = 1.0` can be used to obtain overloaded branches.

`br_status` can be set to `1` (default) or `0` and specifies whether active (`1`) or inactive branches (`0`) should be returned.
"""
function get_branches(
        network_data::Dict{String,<:Any}; 
        pf_type = "MVA-loading", # or "MW-loading", "Mvar-loading"
        min_loading = 0.0, # minimum loading of branches
        br_status = 1 # active or inactive branches
    )

    ### Check whether network_data contains a DC-PF solution
    if network_data["pf_model"] == :dc
        pf_type = "MW-loading" # use branch loadings due to active power
    end

    # branches = [
    #     (i, br[pf_type], br["transformer"]) for (i, br) in network_data["branch"] if br["br_status"] == 1 && br[pf_type] >= min_loading
    # ]
    if br_status == 1
        branches = [
            br["index"] for br in values(network_data["branch"]) if 
            br["br_status"] == br_status && br[pf_type] >= min_loading
        ]
    elseif br_status == 0
        branches = [
            br["index"] for br in values(network_data["branch"]) if 
            br["br_status"] == br_status
        ]
    else
        throw(ArgumentError("Unknown branch status $(br_status)!"))
    end

    return branches
end

"""
    get_cc_data(network_data::Dict{String,<:Any}, cc::Set{Int64})
"""
function get_cc_data(
        network_data::Dict{String,<:Any}, cc::Set{Int64}; name = "cc"
    )

    cc_network_data = Dict{String,Any}(
        "baseMVA" => network_data["baseMVA"],
        "per_unit" => network_data["per_unit"],
        "source_type" => network_data["source_type"],
        "name" => name,
        "source_version" => network_data["source_version"],
        "bus" => Dict{String,Any}(),
        "gen" => Dict{String,Any}(),
        "load" => Dict{String,Any}(),
        "shunt" => Dict{String,Any}(),
        "branch" => Dict{String,Any}(),
        "dcline" => Dict{String,Any}(),
        "storage" => Dict{String,Any}(),
        "switch" => Dict{String,Any}()
    )
    
    for b in cc
        cc_network_data["bus"]["$b"] = network_data["bus"]["$b"]
        
    end
end

#*------------------------------------------------------------------------------

#=
Deactivates branches that have been overloaded due to a power flow redistribution (flow/capacity > 1.). The branches are identified by their string indices in branch_ids. The status of these branches is set to 0 in network_data. Parallel branches are not automatically deactivated. Afterwards PowerModels function simplify_network! is run in order to remove dangling buses etc. that might occur due to the failures.
=#
function disable_branches!(
        network_data::Dict{String,<:Any}, 
        branch_ids::Array{Int64,1}
    )
    ### Go through branches to deactivate
    for id in branch_ids
        network_data["branch"][id]["br_status"] = 0 # deactivate branch
    end

    simplify_network!(network_data) # remove dangling buses etc.

    return nothing
end

"""
    deactivate_overloads!(network_data::Dict{String,<:Any})
"""
function deactivate_overloads!(network_data::Dict{String,<:Any}, f=0)
    ### Get overloaded active branches and deactivate them
    overloaded_br = get_branches(network_data, min_loading=1.)
    if isempty(overloaded_br) == false
        info(LOGGER, 
            "Deactivating the following $(length(overloaded_br)) overloaded branches: $overloaded_br"
        )
    else
        info(LOGGER, "No branches are overloaded.")
    end
    for br in overloaded_br
        network_data["branch"]["$br"]["br_status"] = 0
        network_data["branch"]["$br"]["failure_reason"] = "overload"
    end

    simplify_network!(network_data)

    ### Add new supplied load and the overloaded branches to history
    if haskey(network_data, "history")
        network_data["history"][f][length(network_data["history"][f])+1] = (
            get_total_load(network_data)[1], "overload", overloaded_br
        )
    end

    return overloaded_br
end

#=
Function that deactivates (overhead) transmission lines identified by their indices (string) in tl_ids that have been destroyed by a hurricane. For any given line index all parallel lines also become deactivated. The status of these lines is set to 0 in network_data. Afterwards PowerModels function simplify_network! is run in order to remove dangling buses etc. that might occur due to the failures.
=#
function destroy_tl!(
        network_data::Dict{String,<:Any}, tl_ids::Array{String,1}, f = 0
    )

    ### Go through transmission lines to destroy
    for id in tl_ids
        ## Deactivate id and all parallel lines
        from = network_data["branch"][id]["f_bus"]
        to = network_data["branch"][id]["t_bus"]
        ## Find all parallel branches
        ids = findall(
            b -> (b["f_bus"], b["t_bus"]) == (from, to), 
            network_data["branch"]
        )
        for idx in ids
            network_data["branch"][idx]["br_status"] = 0
            network_data["branch"][idx]["failure_reason"] = "wind"
        end
    end

    simplify_network!(network_data) # remove dangling buses etc.

    ### Add new supplied load and destroyed lines to history
    if haskey(network_data, "history")
        network_data["history"][f][length(network_data["history"][f])+1] = (
            get_total_load(network_data)[1], "wind", parse.(Int64, tl_ids)
        )
    end

    return nothing
end

#=
Deactivates all components (generator, loads, etc.) in a connected component.
=#
function _deactivate_cc!(
        network_data::Dict{String,<:Any},
        cc::Set{Int64}, # set of bus indices in connected component
        active_cc_gens::Array{Int64,1}, 
        active_cc_loads::Array{Int64,1};
        reason = "overproduction" # reason for deactivation
    )

    ### Deactivate all active generators in connected component
    for g in active_cc_gens
        network_data["gen"]["$g"]["gen_status"] = 0
    end

    ### Deactivate all active loads in connected component
    for l in active_cc_loads
        network_data["load"]["$l"]["status"] = 0
    end

    ### Deactivate all branches in cc and save the failure reason
    for br in values(network_data["branch"])
        if br["br_status"] == 1 && br["f_bus"] ∈ cc && br["t_bus"] ∈ cc
            br["br_status"] = 0
            br["failure_reason"] = reason
        end
    end

    ### Deactivate dangling buses in cc
    simplify_network!(network_data)
    
    return nothing
end

#*------------------------------------------------------------------------------

"""
    check_voltage_bounds(network_data::Dict{String,Any}; kwargs...)

Checks whether active buses in `network_data` have voltage magnitudes outside of their tolerance interval (usually ranging from 0.9 p.u. to 1.1 p.u.) and returns a dictionary with the bus indices as keys and the respective active loads connected to the bus as values (PQ-generators are excluded). ITCPG's logger prints a `warn` message everytime a voltage violation is detected.

# Keyword Arguments

`limit` can be set to `"lower"`, `"upper"` or `"both"` (default) and specifies which voltage bound should be considered. If e.g. `limit = "lower"` is chosen, only buses with under-voltages are returned.

`filter_active_loads` can be set to `true` or `false` (default) and specifies whether only buses with active loads connected to them should be returned (useful for under-voltage load shedding). 

See also: [`run_outer_ac_pf!`](@ref), [`run_uvls!`](@ref)
"""
function check_voltage_bounds(
        network_data::Dict{String,<:Any}; 
        limit = "both", # considered voltage bounds
        filter_active_loads = false # whether to filter buses with active loads
    )

    vv_buses = Dict{String,Array{String,1}}()
    ### Go through buses and check their voltage magnitudes
    for (key, bus) in network_data["bus"]
        vm = bus["vm"]
        if limit == "both" || limit == "lower"
            if vm < bus["vmin"] && bus["bus_type"] != 4
                warn(LOGGER, 
                    "Bus $key (type $(network_data["bus"][key]["bus_type"])) has a voltage magnitude of |V|=$vm that is below its lower bound of $(bus["vmin"])" 
                )
                ### Get string indices of active loads connected to the bus
                connected_loads = [
                    l_id for (l_id, l) in network_data["load"] if 
                    l["load_bus"] == parse(Int64, key) && l["status"] == 1 && 
                    haskey(l, "is_gen") == false # exclude PQ-generators
                ]
                vv_buses[key] = connected_loads
            end
        end
        if limit == "both" || limit == "upper"
            if vm > bus["vmax"] && bus["bus_type"] != 4
                warn(LOGGER, 
                    "Bus $key (type $(network_data["bus"][key]["bus_type"])) has a voltage magnitude of |V|=$vm that is above its upper bound of $(bus["vmax"])" 
                )
                ### Get string indices of active loads connected to the bus
                connected_loads = [
                    l_id for (l_id, l) in network_data["load"] if 
                    l["load_bus"] == parse(Int64, key) && l["status"] == 1 && 
                    haskey(l, "is_gen") == false # exclude PQ-generators
                ]
                vv_buses[key] = connected_loads
            end
        end
    end

    ### Check whether to exclude buses without active loads
    if filter_active_loads == true
        filter!(x -> isempty(last(x)) == false, vv_buses)
    end

    return vv_buses
end

"""
    check_gen_bounds(network_data::Dict{String,<:Any}, tol=1.0e-7; mode=:individual)

Checks whether active and reactive power dispatches of generators are within their respective limits according to the tolarance `tol`. If active power violations exist, an `ErrorException` is thrown. If reactive power violations exist, only a warning is returned via the logger.

# Keyword Arguments

`mode` can be either chosen as `:aggregated` (default) or `:individual`. In the case of `:aggregated`, lower and upper capacity limits are summed up for all generators connected to a bus. It is then checked whether the aggregated dispatch of these generators lies within in these bounds. This may omit warnings that would be produced with `mode = :individual`, which checks each generator limit separately. Note that PowerModels.jl distributes the dispatch needed at a slack bus equally between all connected generators. This would be tolerated with `mode = :aggregated` as long as the aggregated generators limits are respected. 
"""
function check_gen_bounds(
        network_data::Dict{String,<:Any}, 
        tol = 1.0e-7; 
        mode = :aggregated,
        if_violation = :error,
        kwargs...
    )

    if mode == :aggregated
        for bus in values(network_data["bus"])
            if bus["bus_type"] == 2 || bus["bus_type"] == 3 # PV- or Vθ-bus
                ### Get connected and active generators
                connected_active_gens = [
                    g for g in values(network_data["gen"]) if
                    g["gen_bus"] == bus["index"] && g["gen_status"] == 1
                ]
                ### Calculate aggregated dispatch and aggregated bounds
                sum_pmin, sum_pmax, sum_pg = 0.0, 0.0, 0.0 # active power
                sum_qmin, sum_qmax, sum_qg = 0.0, 0.0, 0.0 # reactive power
                for g in connected_active_gens
                    sum_pmin += g["pmin"]
                    sum_pmax += g["pmax"]
                    sum_pg += g["pg"]
                    sum_qmin += g["qmin"]
                    sum_qmax += g["qmax"]
                    sum_qg += g["qg"]
                end
                ### Check for violations
                if sum_pg < sum_pmin && !isapprox(sum_pg, sum_pmin, atol=tol)
                    if if_violation == :error
                        ### Save the current network for later analysis
                        path = pwd() * "/ndd_gen_lim_violation_" * randstring(5) * ".jld2"
                        save(path, "network_data", network_data)
                        throw(ErrorException(
                            "END: Aggregated active power dispatch of $sum_pg at bus $(bus["index"]) violates aggregated lower bound of $(sum_pmin)! According to the tolerance of $tol, this is not tolerated."
                        ))
                    elseif if_violation == :warning
                        warn(LOGGER,
                            "Aggregated active power dispatch of $sum_pg at bus $(bus["index"]) violates aggregated lower bound of $(sum_pmin)!"
                        )
                    else
                        throw(ArgumentError(
                            "Unknown if_violation $if_violation."
                        ))
                    end
                end
                if sum_pg > sum_pmax && !isapprox(sum_pg, sum_pmax, atol=tol)
                    if if_violation == :error
                        ### Save the current network for later analysis
                        path = pwd() * "/ndd_gen_lim_violation_" * randstring(5) * ".jld2"
                        save(path, "network_data", network_data)
                        throw(ErrorException(
                            "END: Aggregated active power dispatch of $sum_pg at bus $(bus["index"]) violates aggregated upper bound of $(sum_pmax)! According to the tolerance of $tol, this is not tolerated."
                        ))
                    elseif if_violation == :warning
                        warn(LOGGER,
                            "Aggregated active power dispatch of $sum_pg at bus $(bus["index"]) violates aggregated upper bound of $(sum_pmax)!"
                        )
                    else
                        throw(ArgumentError(
                            "Unknown if_violation $if_violation."
                        ))
                    end
                end
                if sum_qg < sum_qmin && !isapprox(sum_qg, sum_qmin, atol=tol)
                    warn(LOGGER,
                        "Aggregated reactive power dispatch of $sum_qg at bus $(bus["index"]) violates aggregated lower bound of $(sum_qmin)!"
                    )
                end
                if sum_qg > sum_qmax && !isapprox(sum_qg, sum_qmax, atol=tol)
                    warn(LOGGER,
                        "Aggregated reactive power dispatch of $sum_qg at bus $(bus["index"]) violates aggregated upper bound of $(sum_qmax)!"
                    )
                end
            end
        end
    elseif mode == :individual
        for (i, gen) in network_data["gen"]
            if gen["gen_status"] == 1
                pmin, pmax = gen["pmin"], gen["pmax"]
                qmin, qmax = gen["qmin"], gen["qmax"]
                pg, qg = gen["pg"], gen["qg"]
                if pg < pmin && !isapprox(pg, pmin, atol=tol)
                    throw(ErrorException(
                        "END: Active power dispatch of $pg at generator $i violates lower bound of $(pmin)! According to the tolerance of $tol, this is not tolerated."
                    ))
                end
                if pg > pmax && !isapprox(pg, pmax, atol=tol)
                    throw(ErrorException(
                        "END: Active power dispatch of $pg at generator $i violates upper bound of $(pmax)! According to the tolerance of $tol, this is not tolerated."
                    ))
                end
                if qg < qmin && !isapprox(qg, qmin, atol=tol)
                    warn(LOGGER,
                        "Reactive power dispatch of $qg at generator $i violates lower bound of $(qmin)!"
                    )
                end
                if qg > qmax && !isapprox(qg, qmax, atol=tol)
                    warn(LOGGER,
                        "Reactive power dispatch of $qg at generator $i violates upper bound of $(qmax)!"
                    )
                end
            end
        end
    else
        throw(ArgumentError(
            "Unknown mode $(mode) for checking generator limits!"
        ))
    end

    info(LOGGER,
        "All active power limits of generators are respected."
    )

    return nothing
end

function check_gen_bounds(
        network_data::Dict{String,<:Any},
        cc::Set{Int64}, # set of bus indices in connected component
        cc_i::Int64; # index/number of connected components
        power = "active" # "active": active power, "both": reactive power too
    )

    cc_slack = [
        b["index"] for b in values(network_data["bus"]) if 
        b["bus_type"] == 3 && b["index"] ∈ cc
    ]
    
    if length(cc_slack) != 1
        warn(LOGGER, "(Sub-)Grid #$cc_i has $(length(cc_slack)) slack buses!")
    end

    for (i, gen) in network_data["gen"]
        if gen["gen_bus"] ∈ cc && gen["gen_status"] == 1
            if gen["gen_bus"] ∈ cc_slack
                info(LOGGER, 
                    "Active power dispatch of slack bus $i in #$cc_i with interval [$(gen["pmin"]), $(gen["pmax"])]: $(gen["pg"])"
                )
            else
                if gen["pg"] > gen["pmax"] || gen["pg"] < gen["pmin"]
                    warn(LOGGER, "Active power dispatch of generator $i in #$cc_i lies outside of interval [$(gen["pmin"]), $(gen["pmax"])]: $(gen["pg"])")
                end
                if power == "both"
                    if gen["qg"] > gen["qmax"] || gen["qg"] < gen["qmin"]
                        warn(LOGGER,
                            "Reactive power dispatch of generator $i lies outside of interval [$(gen["qmin"]), $(gen["qmax"])]: $(gen["qg"])"
                        )
                    end
                end
            end
        end
    end

    return nothing
end