#* Functions for modelling the impact of hurricanes on power grids.
#*------------------------------------------------------------------------------

function sim_primary_dmg(
        segfile::String, γ::Float64, N_s::Int64;
        respath::String # where to save results in .jld2 files
    )

    failure_probs = get_tl_failure_probs(segfile, γ) # failure probabilities
    random_numbers = deepcopy(failure_probs) # dictionary for random numbers 

    ### Simulate individual scenarios
    result_dict = Dict(
        s => Array{Tuple{String,Int64},1}() for s in 1:N_s
    )
    for s in 1:N_s
        primary_dmg = Array{Tuple{String,Int64},1}() # array for damages   
        eval_p_fail!(primary_dmg, failure_probs, random_numbers)
        result_dict[s] = primary_dmg
    end

    ### Save result
    save(respath, "result", result_dict)

    return nothing
end

function sim_primary_dmg(segfile::String, γ::Float64)
    failure_probs = get_tl_failure_probs(segfile, γ) # failure probabilities
    random_numbers = deepcopy(failure_probs) # dictionary for random numbers 
    primary_dmg = Array{Tuple{String,Int64},1}() # array for line failures    

    eval_p_fail!(primary_dmg, failure_probs, random_numbers)

    return primary_dmg
end

function eval_p_fail!(
        primary_dmg::Array{Tuple{String,Int64},1},
        failure_probs::Dict{String,OrderedDict{Int64,Array{Float64,1}}},
        random_numbers::Dict{String,OrderedDict{Int64,Array{Float64,1}}};
        immune_tl = String[] # optional lines that should be considered immune
    )

    for (i, data) in failure_probs
        if i ∉ immune_tl
            for (f, p_fail) in data
                rand!(random_numbers[i][f])
                if any(random_numbers[i][f] .< p_fail) || any(p_fail .>= 1.0)
                    push!(primary_dmg, (i, f))
                    break
                end
            end
        end
    end

    return primary_dmg
end

function get_tl_failure_probs(segfile::String, γ::Float64)
    failure_probs = Dict{String,OrderedDict{Int64,Array{Float64,1}}}()

    ### Read wind loads and exclude lines and frames with vanishing wind forces
    jldopen(segfile, "r") do file
        for (i, tl) in file["seg_data"]
            ### Filter frames with non-zero wind loads
            tl_filtered = filter(wl->!iszero(last(wl)), tl["windloads"])
            if !isempty(tl_filtered)
                nonzero_wl = OrderedDict{Int64,Array{Float64,1}}(
                    f => wl_arr for (f, wl_arr) in tl_filtered
                )
                ### Add entries in chronological (sorted) order
                failure_probs[i] = sort(nonzero_wl)
            end
        end
    end

    ### Scale wind loads with γ to obtain failure probabilities
    map!(
        x -> OrderedDict{Int64,Array{Float64,1}}(f => γ*arr for (f, arr) in x), 
        values(failure_probs)
    )

    return failure_probs
end

### Draws random values for γ out of the specified range for each overhead line
function get_tl_failure_probs(segfile::String, γ::Vector{Float64})
    @assert length(γ) == 2 (
        "Input vector of length $(length(γ)) should only contain two values defining the upper and lower bound!"
    )

    failure_probs = Dict{String,OrderedDict{Int64,Array{Float64,1}}}()
    
    ### Read wind loads and exclude lines and frames with vanishing wind forces
    jldopen(segfile, "r") do file
        for (i, tl) in file["seg_data"]
            ### Filter frames with non-zero wind loads
            tl_filtered = filter(wl->!iszero(last(wl)), tl["windloads"])
            if !isempty(tl_filtered)
                ### Draw random γ-value for current line
                γ_rand = rand(Uniform(γ[1], γ[2]))
                nonzero_wl = OrderedDict{Int64,Array{Float64,1}}(
                    f => γ_rand * wl_arr for (f, wl_arr) in tl_filtered
                )
                ### Add entries in chronological (sorted) order
                failure_probs[i] = sort(nonzero_wl)
            end
        end
    end

    return failure_probs
end

#*------------------------------------------------------------------------------

#=
Calculates wind loads (wind force / breaking force) for overhead transmission lines in the network data dictionary and a given .nc file containing wind data. The overhead transmission lines are identified according to the chosen mode ((:sum or :single, see get_underground_tl) and divided into segments between towers. The results are returned in form of a dictionary (see calc_overhead_tl_segments).
=#
function calc_overhead_tl_windloads(
        network_data::Dict{String,<:Any},
        windfile::String, # .nc file containing wind data
        d_twrs = 161.0;
        mode = :sum # :sum or :single
    )

    ### Read wind data
    wind_lons, wind_lats, wind_speeds = get_windfield(windfile)

    ### Identify overhead lines and divide them into segments between towers
    overhead_tl_segments = calc_overhead_tl_segments(
        network_data, d_twrs, mode=mode
    )

    ### Assign location indices to segments that identify the local wind speed
    _assign_loc_indices!(overhead_tl_segments, wind_lons, wind_lats)

    ### Go through time steps in the wind data and calculate wind loads
    for f in 1:size(wind_speeds, 3)
        wind_frame = @view wind_speeds[:,:,f]
        _calc_seg_windloads!(overhead_tl_segments, wind_frame, f)
    end

    return overhead_tl_segments # dictionary with all information
end

#*------------------------------------------------------------------------------

#= 
Returns the longitude and latitude coordinates of the wind field together with the whole time series of wind speeds.
=#
function get_windfield(filepath::String)
    wind_lons = round.(ncread(filepath, "lon"), digits=6)
    wind_lats = round.(ncread(filepath, "lat"), digits=6)
    wind_speeds = ncread(filepath, "wind")

    return wind_lons, wind_lats, wind_speeds
end

function get_windfield(filepath::String, frame::Int64)
    wind_lons, wind_lats, wind_speeds = get_windfield(filepath)
    return wind_lons, wind_lats, wind_speeds[:, :, frame]
end

#=
Function that returns the number of frames (time steps) in the given .nc data file and the maximum wind speed.
=#
function get_winddata(filepath::String)
    wind = ncread(filepath, "wind")
    N_frames = size(wind, 3)
    max_ws = maximum(wind)
    return N_frames, max_ws
end

#*------------------------------------------------------------------------------

#=
Identifies overhead transmission lines in the network data dictionary according to the chosen mode (:sum or :single, see get_underground_tl) and divides them into segments between towers. The towers are assumed to be d_twrs (in m) apart and the resulting segments have a length (l_seg) close to d_twrs. The resulting segments are returned in form of a dictionary with entries explained in the code below.
=#
function calc_overhead_tl_segments(
        network_data::Dict{String,<:Any}, 
        d_twrs = 161.0;
        mode = :sum # :sum or :single
    )

    ### Get (string) indices of underground transmission lines
    underground_tl = get_underground_tl(network_data, mode=mode)
    
    unique_overhead_tl = [
        i for (i, b) in network_data["branch"] 
        if b["transformer"] == false && i ∉ underground_tl 
        && b["source_id"][end] == "1 " # parallel circuits excluded
    ] # string indices of unique overhead transmission lines

    seg_data = Dict{String,Dict{String,Any}}() # dictionary for segment data

    for i in unique_overhead_tl
        branch = network_data["branch"][i]
        f_bus, t_bus = branch["f_bus"], branch["t_bus"]
        lon_f = network_data["bus"]["$f_bus"]["bus_lon"]
        lat_f = network_data["bus"]["$f_bus"]["bus_lat"]
        lon_t = network_data["bus"]["$t_bus"]["bus_lon"]
        lat_t = network_data["bus"]["$t_bus"]["bus_lat"]

        L, N_seg, l_seg, seg_lons, seg_lats = _calc_tl_segments(
            lon_f, lat_f, lon_t, lat_t, d_twrs
        ) # data for transmission line segments

        ### Add data to dictionary
        seg_data[i] = Dict(
            "f_bus" => f_bus, # index (Int64) of from-bus
            "lon_f" => lon_f, # longitude of from-bus
            "lat_f" => lat_f, # latitude of from-bus
            "t_bus" => t_bus, # index (Int64) of to-bus
            "lon_t" => lon_t, # longitude of to-bus
            "lat_t" => lat_t, # latitude of to-bus
            "L" => L, # calculated length of transmission line
            "N_seg" => N_seg, # number of segments between towers
            "l_seg" => l_seg, # actual length of segments (close to d_twrs)
            "seg_lons" => seg_lons, # longitudes of segment-half-way points
            "seg_lats" => seg_lats, # latitudes of segment-half-way points
            "windloads" => Dict{Int64,Array{Float64,1}}() # for wind loads
        )
    end

    return seg_data
end

#= 
Function that divides a transmission line (tl) between a "from-bus" with coordinates (lon_f, lat_f) in degrees and a "to-bus" (lon_t, lat_t) into segments of wanted length d_twrs (average distance between transmission towers in meter) and returns the half-way points (longitude and latitude coordinates) of all segments together with length L of the transmission line (calculated using the haversine formula), the number of segments (N_twrs+1), and the actual length of segments l_seg.
=#
function _calc_tl_segments(
        lon_f::Float64, lat_f::Float64, 
        lon_t::Float64, lat_t::Float64,
        d_twrs = 161.0
    )

    R = 6371000 # earth radius
    λf, φf, λt, φt = deg2rad.([lon_f, lat_f, lon_t, lat_t])

    ### Calculate haversine distance between "from-" and "to-bus"
    h = sin((φt-φf)/2)^2 + cos(φf) * cos(φt) * sin((λt-λf)/2)^2 # haversine
    δ = 2 * atan(sqrt(h), sqrt(1-h)) # angular distance in radians
    L = R * δ # transmission line length

    N_twrs = round(L/d_twrs) # number of transmission towers
    l_seg = L/N_twrs # actual length of line segments

    ### Calulate the fractions of the line at which the half-way points lie
    f = 1/N_twrs
    f_seg = [(i+0.5)*f for i in 0:N_twrs]

    a = sin.((1 .- f_seg)*δ)/sin(δ)
    b = sin.((f_seg*δ))/sin(δ)
    x = a*cos(φf)*cos(λf) + b*cos(φt)*cos(λt)
    y = a*cos(φf)*sin(λf) + b*cos(φt)*sin(λt)
    z = a*sin(φf) + b*sin(φt)

    ### Calculate longitudes and latitudes of all half-way points in radians
    λ_seg = atan.(y, x) 
    φ_seg = atan.(z, sqrt.(x.^2 + y.^2))

    return L, Int64(N_twrs+1), l_seg, rad2deg.(λ_seg), rad2deg.(φ_seg)
end


#*------------------------------------------------------------------------------

#=
Assigns location indices to transmission line segments that help to identify local wind speeds for the calculation of wind loads. The location indices are added to the data dictionary seg_data.
=#
function _assign_loc_indices!(
        seg_data::Dict{String,<:Any},
        wind_lons::Array{Float64,1}, # longitudes in wind field
        wind_lats::Array{Float64,1} # latitudes in wind field
    )

    ### Go through overhead transmission lines
    for tl_data in collect(values(seg_data))
        N_seg = tl_data["N_seg"]
        lon_indices = zeros(Int64, N_seg)
        lat_indices = zeros(Int64, N_seg)
        ### Go through segments and identify their position in the wind field
        for i in 1:N_seg
            seg_lon = tl_data["seg_lons"][i]
            seg_lat = tl_data["seg_lats"][i]
            lon_indices[i] = argmin(abs.(wind_lons .- seg_lon))
            lat_indices[i] = argmin(abs.(wind_lats .- seg_lat))
        end
        tl_data["loc_indices"] = [loc for loc in zip(lon_indices, lat_indices)]
    end

    return nothing
end

#*------------------------------------------------------------------------------

#=
Calculates wind loads for all overhead transmission line segments in the segment data dictionary containing location indices (see _assign_loc_indices!) and a given time step (frame) of the wind field. The results are added to seg_data.
=#
function _calc_seg_windloads!(
        seg_data::Dict{String,<:Any}, 
        wind_frame::SubArray, # slice of wind speed time series
        f::Int64 # wind frame number
    )

    ### Go through overhead transmission lines
    for (key, tl_data) in seg_data
        windloads = zeros(tl_data["N_seg"])
        ### Go through unique segment location indices and calculate wind loads
        for loc in unique(tl_data["loc_indices"])
            ws = wind_frame[CartesianIndex(loc)] # local wind speed
            wl = _calc_windload(ws, tl_data["l_seg"]) # local wind load
            ### Add this wind load to all segments with these location indices
            for i in findall(l -> l == loc, tl_data["loc_indices"])
                windloads[i] = wl
            end
        end
        if !iszero(windloads)
            tl_data["windloads"][f] = windloads # add result to dictionary
        end
    end
    
    return nothing
end

#= 
Function that calculates the failure probabilities of transmission line segments according to the local wind speed and the length of the segments.
=#
function _calc_windload(v::Float64, l::Float64)
    ### Parameters in wind force design equation
    Q, k_z, C_f, d, F_brk = [0.613, 1.17, 0.9, 0.03, 152130]

    ### Gust response factor for line segment
    E_W = 4.9*sqrt(0.005)*(33/70)^(1/7)
    G_WRF = 1 + 2.7*E_W/sqrt(1 + 0.8/67.056*l)

    ### Convert wind speeds to an effective height of h = 70ft = 21.336m
    v *= 1.16 # conversion factor ln(21.336/0.1)/ln(100) ≈ 1.16 from 10m to h

    ### Calculate wind force according to ASCE design equation
    F_wind = Q * k_z * C_f * G_WRF * d * l * v^2
    ### Return wind load on all segment
    return F_wind / F_brk
end
