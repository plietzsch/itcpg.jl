# ITCPG

A Julia package for simulating the Impact of Tropical Cyclones (TC) on Power Grids (ITCPG). The power grid modeling is based on [PowerModels.jl](https://github.com/lanl-ansi/PowerModels.jl) and includes both an AC and a DC model.

## Installation

Add the package using Julia's package manager and the URL of this repository:

```julia-repl
(v1.6) pkg> add https://gitlab.pik-potsdam.de/plietzsch/itcpg.jl
```

Afterwards, `using ITCPG` should allow you to use all exported functions.

## Getting Started

Given a power grid data set `pg_data.m` in the Matpower file format (or PTI RAW file `pg_data.RAW`), one can build a PowerModels conform network data dictionary using `build_network_data`:

```julia
using ITCPG

network_data = build_network_data("path/to/pg_data.m")
```

The simulations require the geographic locations of all buses. Since these are not part of the Matpower or PTI RAW format, they have to be added to `network_data` using a CSV file of the form:

| Number      | SubLatitude | SubLongitude |
| :---------: | :---------: | :----------: |
| 1           | 31.9        | -102.2       |
| 2           | 32.33       | -100.1       |

Here, the "Number" column contains the ID's of buses in the data set and "SubLatitude" and "SubLongitude" contain the respective latitude and longitude in degrees.

If `locs.csv` is a CSV file with the form above, the geographic bus locations can be added to `network_data` using `add_locs!(network_data, "path/to/locs.csv")`.
Alternatively, the CSV file can also be passed to `build_network_data`:

```julia
network_data = build_network_data("path/to/pg_data.m", locfile="locs.csv")
```

### Running Simulations

Given a TC data set `tc_data.nc` in NetCDF format, the first step is to calculate the wind loads acting on overhead transmission lines in the power grid:

```julia
seg_data = calc_overhead_tl_windloads(network_data, "path/to/tc_data.nc")
```

Here, `seg_data` is a dictionary that contains all the information on the wind loads acting on individual line segments. It is best saved as, e.g., a JLD2 file, such that it can be used for simulations later.

Next, scenarios of wind-induced line failures (primary damages) can be calculated for a specific value `gamma` of the scaling parameter:

```julia
primary_dmg = sim_primary_dmg(seg_data, gamma)
```

Given a specific primary damage scenario `primary_dmg`, the induced cascading failures can be calculated using the DC power flow model (`pf_model=:dc`) or AC power flow model (`pf_model=:ac`):

```julia
calc_secondary_dmg!(network_data, primary_dmg, pf_model=:dc)
```

This modifies `network_data` according to the arising outages. Inspecting `network_data` after the simulation finished can give insights into the final state of the power grid.

## Examples

Exemplary scenario of [Hurricane Claudette](http://ibtracs.unca.edu/index.php?name=v04r00-2003188N11307) using the [ACTIVSg2000](https://electricgrids.engr.tamu.edu/electric-grid-test-cases/activsg2000/) data set of Texas:
![Link broken](examples/Claudette_impact.gif)
